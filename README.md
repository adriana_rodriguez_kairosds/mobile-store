![Image text](/src/assets/images/logo2.png)

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Scripts](#scripts)

## General Info
***
Mobile store simulation application


## Screenshot
![List Products](/src/assets/images/screen1.png)
![Detail Product](/src/assets/images/screen2.png)


## Technologies
***
A list of technologies used within the project:
* [React](https://es.reactjs.org)
* [Axios](https://github.com/axios/axios)
* [Testing Library](https://testing-library.com)
* [React router dom](https://github.com/remix-run/react-router)
* [Sweet alert](https://sweetalert2.github.io/)


## Installation
***
Steps for installation:
```
$ git clone git@gitlab.com:adriana_rodriguez_kairosds/mobile-store.git
$ cd mobile-store.git
$ npm install
$ npm start
```

## Scripts
***
The project contains the following script, to be able to manage the application:
* START - Development mode
* BUILD - Compilation for Production mode
* TEST - Test launch
* LINT - Code check
