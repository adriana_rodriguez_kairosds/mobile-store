export const successMessage = {
  icon: 'success',
  title: 'Nuevo Móvil!!',
  text: 'Has añadido el móvil al carrito de compras.',
  confirmButtonText: 'Aceptar',
  confirmButtonColor: '#E3BA22',
  iconColor: '#28a08a'
}
