export const BASE = 'https://front-test-api.herokuapp.com/'
export const PRODUCTS = 'api/product/'
export const ADD_TO_CART = 'api/cart'
export const requestOptions = {
  headers: {
    'Content-Type': 'application/json'
  }
}
