
export const bread = (path, brand = '', model = '') => {
  return path === '/' ? '' : brand + ' ' + model
}

export const listed = (d, index) => (index === 0 ? d : ', ' + d)

export const isFound = (dato, payload) => {
  return dato.toLowerCase().includes(payload.toLowerCase())
}

export const isTimeValid = (storageCreationTime) => {
  const exptime = new Date(storageCreationTime)
  const now = new Date()
  const timeElapsed = now - exptime
  const segs = 1000
  const mins = segs * 60
  const hours = mins * 60
  const timeInHours = Math.floor(timeElapsed / hours)
  return timeInHours < 1
}

export const isOnlyOne = (array) => {
  return array ? array.length === 1 : null
}
