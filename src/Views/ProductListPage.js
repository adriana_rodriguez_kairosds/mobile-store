import React from 'react'
import { ListItem } from '../components/ListItem'
import { Search } from './../components/Search'

export const ProductListPage = () => {
  return (
    <div id='detail-product'>
      <Search />
      <ListItem />
    </div>
  )
}
