import React, { useContext, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import { Actions } from '../components/Actions'
import { Description } from '../components/Description'
import { ProductContext } from '../context/products/ProductContext'
import { Image } from './../components/Image'

export const ProductDetailsPage = () => {
  const { id } = useParams()
  const productContext = useContext(ProductContext)
  const { getById} = productContext
  useEffect(() => {
    getById(id)
  }, [getById,id])
  

  return (
    <div id='product-description'>
      <Image />
      <div className='info-detail'>
        <Description />
        <Actions />
        <div className='info-detail'>
          <Link to='/'>
            <p>Ver más productos</p>
          </Link>
        </div>
      </div>
    </div>
  )
}
