import * as types from '../../config/types'
import { isFound } from './../../config/helpers'

export function ProductReducer (state, action) {
  const { type, payload } = action

  switch (type) {
    case types.GET_ALL_PRODUCTS:
      return {
        ...state,
        products: payload
      }
    case types.GET_PRODUCT:
      return {
        ...state,
        selectedProduct: payload
      }

    case types.RESET_SEARCH:
      return {
        ...state,
        search: [],
        searchData: payload
      }
    case types.SEARCH_PRODUCTS:

      return {
        ...state,
        search: state.products.filter(
          (p) => isFound(p.brand, payload) || isFound(p.model, payload) || isFound(p.price, payload)
        ),
        searchData: payload
      }
    case types.ADD_PRODUCT_TO_CART:
      return {
        ...state,
        cart: payload
      }
    default:
      return state
  }
}
