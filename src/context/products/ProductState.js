import axios from 'axios'
import { useReducer } from 'react'
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { isTimeValid } from '../../config/helpers'
import { successMessage } from '../../config/messages'
import * as url from './../../config/constants'
import * as types from './../../config/types'
import { ProductContext } from './ProductContext'
import { ProductReducer } from './ProductReducer'
export const ProductState = (props) => {
  const navigate = useNavigate()
  const MySwal = withReactContent(Swal)
  const initialState = {
    products: [],
    cart: 0,
    search: [],
    searchData: '',
    selectedProduct: null
  }
  const [state, dispatch] = useReducer(ProductReducer, initialState)

  const getAll = async () => {
    try {
      if (!JSON.parse(window.localStorage.getItem('mobileStoreProducts'))) {
        const res = await axios.get(
          url.BASE + url.PRODUCTS,
          url.requestOptions
        )
        const storageData = { data: res.data, expTime: new Date() }
        window.localStorage.setItem(
          'mobileStoreProducts',
          JSON.stringify(storageData)
        )
        dispatch({ type: types.GET_ALL_PRODUCTS, payload: res.data })
      } else {
        const storageData = JSON.parse(
          window.localStorage.getItem('mobileStoreProducts')
        )
        if (isTimeValid(storageData.expTime)) {
          dispatch({ type: types.GET_ALL_PRODUCTS, payload: storageData.data })
        } else {
          const res = await axios.get(
            url.BASE + url.PRODUCTS,
            url.requestOptions
          )
          const storageData = { data: res.data, expTime: new Date() }
          window.localStorage.setItem(
            'mobileStoreProducts',
            JSON.stringify(storageData)
          )
          dispatch({ type: types.GET_ALL_PRODUCTS, payload: res.data })
        }
      }
    } catch (error) {
      console.error(error)
    }
  }

  const setSearch = (searchData) => {
    try {
      dispatch({ type: types.SEARCH_PRODUCTS, payload: searchData })
    } catch (error) {
      console.error(error)
    }
  }

  const resetSearch = () => {
    try {
      dispatch({ type: types.RESET_SEARCH, payload: '' })
    } catch (error) {
      console.error(error)
    }
  }

  const getById = async (id) => {
    try {
      const res = await axios.get(
        url.BASE + url.PRODUCTS + id,
        url.requestOptions
      )
      const data = res.data
      dispatch({ type: types.GET_PRODUCT, payload: data })
    } catch (error) {
      navigate('/')
    }
  }

  const addToCart = async (body) => {
    try {
      const res = await axios.post(
        url.BASE + url.ADD_TO_CART,
        body,
        url.requestOptions
      )
      if (res) {
        MySwal.fire(successMessage)
      }
      const cartData = { count: res.data.count, expTime: new Date() }
      if (!JSON.parse(window.localStorage.getItem('mobileStoreCart'))) {
        window.localStorage.setItem(
          'mobileStoreCart',
          JSON.stringify(cartData)
        )
      } else {
        const cartDataLocalStore = JSON.parse(
          window.localStorage.getItem('mobileStoreCart')
        )
        if (isTimeValid(cartDataLocalStore.expTime)) {
          const count = cartDataLocalStore.count + res.data.count
          const newCartData = { ...cartData, count }
          window.localStorage.setItem(
            'mobileStoreCart',
            JSON.stringify(newCartData)
          )
        } else {
          window.localStorage.removeItem('mobileStoreCart')
          const cartData = { count: res.data.count, expTime: new Date() }
          window.localStorage.setItem(
            'mobileStoreCart',
            JSON.stringify(cartData)
          )
        }
        const cart = JSON.parse(window.localStorage.getItem('mobileStoreCart'))
        dispatch({ type: types.ADD_PRODUCT_TO_CART, payload: cart.count })
      }
    } catch (error) {
      console.error(error)
    }
  }

  const getCart = () => {
    if (window.localStorage.getItem('mobileStoreCart')) {
      const cartDataLocalStore = JSON.parse(
        window.localStorage.getItem('mobileStoreCart')
      )
      if (!isTimeValid(cartDataLocalStore.expTime)) {
        window.localStorage.removeItem('mobileStoreCart')
        window.localStorage.setItem(
          'mobileStoreCart',
          JSON.stringify({ count: 0, expTime: new Date() })
      )
      
    } else {
      window.localStorage.setItem(
        'mobileStoreCart',
        JSON.stringify({ count: 0, expTime: new Date() })
      )
    }
    const cart = JSON.parse(window.localStorage.getItem('mobileStoreCart'))
    dispatch({ type: types.ADD_PRODUCT_TO_CART, payload: cart.count })
  }
  }
  return (
    <ProductContext.Provider
      value={{
        products: state.products,
        selectedProduct: state.selectedProduct,
        cart: state.cart,
        search: state.search,
        searchData: state.searchData,
        getAll,
        getById,
        addToCart,
        getCart,
        setSearch,
        resetSearch
      }}
    >
      {props.children}
    </ProductContext.Provider>
  )
}
