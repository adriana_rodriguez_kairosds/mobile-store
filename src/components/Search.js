import React, { useContext, useState } from 'react'
import { ProductContext } from '../context/products/ProductContext'

export const Search = () => {
  const productContext = useContext(ProductContext)
  const [searchTerm, setSearchTerm] = useState('')

  const handleChange = (event) => {
    event.preventDefault()
    setSearchTerm(event.target.value)
    productContext.setSearch(event.target.value)
  }

  return (
    <div id='search'>
      <form>
        <label htmlFor='search'>Search</label>
        <input
          id='search'
          type='search'
          pattern='.*\S.*'
          required
          value={searchTerm}
          onChange={handleChange}
        />
        <span className='caret' />
      </form>
    </div>
  )
}
