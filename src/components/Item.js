import React from 'react'
import { Link } from 'react-router-dom'

export const Item = ({ data }) => {
  const { brand, id, imgUrl, model, price } = data

  return (
    <div className='item'>
      <Link to={'product/' + id}>
        <div className='item-info'>
          <img src={imgUrl} alt={model} />
          <h3>
            <span className='brand'>{brand}</span> {model}
          </h3>

          <div className='price'>{price === '' ? '0' : price} €</div>
        </div>
      </Link>
    </div>
  )
}
