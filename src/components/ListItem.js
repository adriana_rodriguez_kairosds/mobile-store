import React, { useContext, useEffect } from 'react'
import { ProductContext } from '../context/products/ProductContext'
import { Item } from './../components/Item'

export const ListItem = () => {
  const productContext = useContext(ProductContext)
  const { getAll, resetSearch } = productContext
  const { search, searchData, products } = productContext
  const isSearch = searchData.length > 0

  useEffect(() => {
    getAll()
    resetSearch()
  }, [])

  const data = isSearch ? search : products
  return (
    <>
      <div className='result'>

        {isSearch ? `Search results for " ${searchData}"` : null}
      </div>
      <div id='item-list'>
        {data.length > 0
          ? data.map((item) => <Item data={item} key={item.id} />)
          : null}
      </div>
    </>
  )
}
