import React, { useContext } from 'react'
import { listed } from '../config/helpers'
import { ProductContext } from '../context/products/ProductContext'
export const Description = () => {
  let selected = {}
  const productContext = useContext(ProductContext)
  if (productContext?.selectedProduct) {
    selected = productContext?.selectedProduct
  }

  const {
    brand,
    model,
    externalMemory,
    internalMemory,
    price,
    cpu,
    sim,
    os,
    displayType,
    displayResolution,
    displaySize,
    battery,
    weight,
    dimentions,
    ram,
    primaryCamera,
    secondaryCamera
  } = selected

  return (
    <div>
      {' '}
      {brand && (
        <>
          <h1>
            {brand} {model}
          </h1>
          <table>
            <tbody>
              <tr>
                <td>Marca:</td>
                <td>{brand}</td>
              </tr>
              <tr>
                <td>Modelo:</td>
                <td>{model}</td>
              </tr>
              <tr>
                <td>Memoria Externa:</td>
                <td>{externalMemory}</td>
              </tr>
              {typeof internalMemory === 'object' && (
                <tr>
                  <td> Memoria Interna:</td>
                  <td>{internalMemory?.map((d, index) => listed(d, index))}</td>
                </tr>
              )}
              <tr>
                <td>Precio:</td>
                <td>{price.length > 1 ? price : 0} €</td>
              </tr>
              <tr>
                <td>CPU:</td>
                <td>{cpu}</td>
              </tr>
              <tr>
                <td>RAM:</td>
                <td>{ram}</td>
              </tr>
              <tr>
                <td>Sistema Operativo:</td>
                <td>{os}</td>
              </tr>
              <tr>
                <td>SIM:</td>
                <td>{sim}</td>
              </tr>
              <tr>
                <td>Pantalla</td>
                <td>
                  {displayType}. {displaySize}
                </td>
              </tr>
              <tr>
                <td>Resolución de pantalla:</td>
                <td>{displayResolution}</td>
              </tr>
              <tr>
                <td>Bateria:</td>
                <td>{battery}</td>
              </tr>
              <tr>
                <td>Peso:</td>
                <td>{weight} gramos</td>
              </tr>
              <tr>
                <td>Dimensiones:</td>
                <td>{dimentions}</td>
              </tr>
              {typeof primaryCamera === 'object' && (
                <tr>
                  <td>Cámaras:</td>
                  <td>{primaryCamera.map((d, index) => listed(d, index))}</td>
                </tr>
              )}
              {typeof secondaryCamera === 'object' && (
                <tr>
                  <td>Cámaras Secundarias:</td>
                  <td>{secondaryCamera.map((d, index) => listed(d, index))}</td>
                </tr>
              )}
            </tbody>
          </table>
        </>
      )}
    </div>
  )
}
