import React, { useContext, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { ProductContext } from '../context/products/ProductContext'
import cartImage from './../assets/images/cart.svg'
import logoImage from './../assets/images/logo2.png'
import { bread } from './../config/helpers'

export const Header = () => {
  const productContext = useContext(ProductContext)
  const location = useLocation()
  const { getCart} = productContext;
  useEffect(() => {
    getCart()
  }, [])

  return (
    <div id='header'>
      <div className='top'>
        <Link to='/'>
          <img src={logoImage} alt='cart' />
        </Link>
        <div className='cart'>
          <img src={cartImage} alt='cart' /> {productContext.cart}
        </div>
      </div>
      <div className='breadcrumbs'>
        <Link to='/'>Lista de Móviles/</Link>
        <Link to={location.pathname}>{bread(location.pathname, productContext.selectedProduct?.brand, productContext.selectedProduct?.model)}</Link>
      </div>
    </div>
  )
}
