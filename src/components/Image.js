import React, { useContext } from 'react'
import { ProductContext } from '../context/products/ProductContext'

export const Image = () => {
  const productContext = useContext(ProductContext)

  return (
    <div id='image'>
      <img
        src={productContext.selectedProduct?.imgUrl}
        alt={
          productContext.selectedProduct?.brand +
          ' ' +
          productContext.selectedProduct?.model
        }
      />
    </div>
  )
}
