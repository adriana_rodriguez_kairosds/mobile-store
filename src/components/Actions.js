import React, { useContext, useState } from 'react'
import { isOnlyOne } from '../config/helpers'
import { ProductContext } from '../context/products/ProductContext'
import cartImage from './../assets/images/cart.svg'
export const Actions = () => {
  const [color, setColor] = useState()
  const [storage, setStorage] = useState()
  const productContext = useContext(ProductContext)
  const id = productContext.selectedProduct?.id
  const colors = productContext.selectedProduct?.options?.colors
  const storages = productContext.selectedProduct?.options?.storages
  const oneColor = isOnlyOne(colors)
  const theColor = oneColor ? colors[0].code : color

  const oneStorage = isOnlyOne(storages)
  const theStorage = oneStorage ? storages[0].code : storage

  const submitFormHandler = async (event) => {
    event.preventDefault()

    const data = {
      id: productContext.selectedProduct?.id,
      colorCode: theColor,
      storageCode: theStorage
    }

    productContext.addToCart(data)
  }

  return (
    <div id='actions'>
      <form onSubmit={submitFormHandler} className='cart-form'>
        <div className='action-select'>
          <div className='color-select'>
            Color
            <label className='select' forhtml='color'>
              <select
                name='color'
                value={oneColor ? colors[0].code : color}
                onChange={(event) => setColor(event.target.value)}
                required
              >
                <option />
                {oneColor
                  ? (
                    <option value={colors[0].code} key={id}>
                      {colors[0].name}
                    </option>
                    )
                  : (
                      colors &&
                  colors.map((color) => (
                    <option value={color.code} key={color.code}>
                      {color.name}
                    </option>
                  ))
                    )}
              </select>
              <svg>
                <use xlinkHref='#select-arrow-down' />
              </svg>
            </label>
            <svg className='sprites'>
              <symbol id='select-arrow-down' viewBox='0 0 10 6'>
                <polyline points='1 1 5 5 9 1' />
              </symbol>
            </svg>
          </div>
          <div className='storage-select'>
            Almacenamiento
            <label className='select' forhtml='storageCode'>
              <select
                name='storage'
                value={oneStorage ? storages[0].code : storage}
                onChange={(event) => setStorage(event.target.value)}
                required
              >
                <option />

                {oneStorage
                  ? (
                    <option value={storages[0].code} key={id}>
                      {storages[0].name}
                    </option>
                    )
                  : (
                      storages &&
                  storages.map((storage) => (
                    <option value={storage.code} key={storage.code}>
                      {storage.name}
                    </option>
                  ))
                    )}
              </select>
              <svg>
                <use xlinkHref='#select-arrow-down' />
              </svg>
            </label>
            <svg className='sprites'>
              <symbol id='select-arrow-down' viewBox='0 0 10 6'>
                <polyline points='1 1 5 5 9 1' />
              </symbol>
            </svg>
          </div>
        </div>
        <button className='add-to-cart' type='submit'>
          <img src={cartImage} alt='cart' /> Añadir a la cesta
        </button>
      </form>
    </div>
  )
}
