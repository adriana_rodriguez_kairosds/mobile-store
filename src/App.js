import React from 'react'
import { Route, Routes } from 'react-router-dom'
import './App.css'
import { Header } from './components/Header'
import { ProductState } from './context/products/ProductState'
import { ProductDetailsPage } from './Views/ProductDetailsPage'
import { ProductListPage } from './Views/ProductListPage'

function App () {
  return (
    <ProductState>
      <div id='store'>
        <Header />
        <Routes>
          <Route path='/' element={<ProductListPage />} />
          <Route path='products' element={<ProductListPage />} />
          <Route path='products' element={<ProductListPage />} />
          <Route path='product/:id' element={<ProductDetailsPage />} />
          <Route path='*' element={<ProductListPage />} />

        </Routes>
      </div>
    </ProductState>
  )
}

export default App
